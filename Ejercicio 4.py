# Autor: Cinthia Valdez
# email cinthia.valdez@unl.edu.ec
# Desarrollo de ejercicios del capítuolo 4
# Ejercicio 4: Reescribe el programa de caliﬁcaciones del capítulo anterior
# usando una función llamada calcula_calificacion, que reciba una puntuación
# como parámetro y devuelva una caliﬁcación como cadena.
            
def calcula_calificacion (de_0_09) :

    if de_0_09 > 0.9:
        print("Sobresaliente")
    elif de_0_09 > 0.8:
        print("Notable")
    elif de_0_09 > 0.7:
        print("Bién")
    elif de_0_09 > 0.6:
        print("Suficiente")
    elif de_0_09 <= 0.6:
      print("Insuficiente")
    else:
        print("puntuacion incorrecta")
try:

    print("introduzca una puntuación")
    de_0_09 = float(input())
    calcula_calificacion (de_0_09)
except:
    print("Valor ingresado incorrecto, (Ingrese un número)")
