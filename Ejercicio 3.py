# Autor: Cinthia Valdez
# email cinthia.valdez@unl.edu.ec
# Desarrollo de ejercicios del capituolo 4

# Ejercicio 3: Reescribe el programa de cálculo del salario, con tarifa-ymedia para
# las horas extras, y crea una función llamada calculo_salario que reciba dos
# parámetros (horas y tarifa).

def calculo_salario (horas , tarifa ):

    salario = float(horas * tarifa)

    if horas > 40:
        horasex = horas - 40
        salario = (horas - horasex) * tarifa
        pagoext = (tarifa * 1.5) * horasex
        pagototal = pagoext + salario
        print('Su salario con horas extras es de  : ', pagototal)
    elif horas <= 40:
        print('El salario sin horas extras es de : ', salario)

print("ingrese el numero de horas que trabaja")
horas = (int(input()))
print("Ingrese su tarifa díaria")
tarifa = float(input())
calculo_salario (horas, tarifa)
