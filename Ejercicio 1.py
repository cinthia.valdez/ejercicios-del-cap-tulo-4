# Autor: Cinthia Valdez
# email cinthia.valdez@unl.edu.ec
# Desarrollo de ejercicios del capituolo 4

# Ejercicio 1  ¿Cuál es la utilidad de la palabra clave “def” en Python?

# a) Es una jerga que signiﬁca “este código es realmente estupendo”
# b) Indica el comienzo de una función
# c) Indica que la siguiente sección de código indentado debe ser
#    almacenada para usarla más tarde.
# d) b y c son correctas ambas.
# e) Ninguna de las anteriores.

# RESPUESTA : La respuesta correcta es elliteral (c) ya def es un apalabara clave para
# definir una función que puede ser llamada. 