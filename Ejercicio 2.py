# Autor: Cinthia Valdez
# email cinthia.valdez@unl.edu.ec
# Desarrollo de ejercicios del capituolo 4

# Ejercicio 2 : ¿Qué mostrará en pantalla el siguiente programa Python?
def fred():
    print("Zap")
def jane():
    print("ABC")
jane()
fred()
jane()
# a) Zap ABC jane fred jane
# b) Zap ABC Zap
# c) ABC Zap jane
# d) ABC Zap ABC
# e) Zap Zap Zap
# Respuesta: el linteral que va a mostrar este programa será el (d) ABC Zap ABC



